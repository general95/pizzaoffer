PizzaOffer


Build & Test:

[![GitLab pipeline status](https://gitlab.com/general95/pizzaoffer/badges/master/pipeline.svg)](https://gitlab.com/general95/pizzaoffer/commits/master)

[![Build Status](https://alireza-dl.visualstudio.com/PizzaOffer/_apis/build/status/PizzaOffer-CI1?branchName=master)](https://alireza-dl.visualstudio.com/PizzaOffer/_build/latest?definitionId=4&branchName=master)

Linux:

[![Build Status](https://alireza-dl.visualstudio.com/PizzaOffer/_apis/build/status/PizzaOffer-CI1?branchName=master&jobName=Job&configuration=linux)](https://alireza-dl.visualstudio.com/PizzaOffer/_build/latest?definitionId=4&branchName=master)

Windows:

[![Build Status](https://alireza-dl.visualstudio.com/PizzaOffer/_apis/build/status/PizzaOffer-CI1?branchName=master&jobName=Job&configuration=windows)](https://alireza-dl.visualstudio.com/PizzaOffer/_build/latest?definitionId=4&branchName=master)
